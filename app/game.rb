# frozen_string_literal: true

TITLE_WIDTH = 700
TITLE_HEIGHT = 150

class Lander
  def initialize(args)
    @args = args
    @fuel = 500
    @pad ||= make_pad
    @gnd ||= Ground.new(@pad)
    @player ||= Player.new
    @center_x = 640
    @center_y = 384
    @gravity = 0.09
    @score = 0
    @font = 'fonts/pico8-mono.ttf'
    @explode = Explode.new(90) if @args.state.tick_count.zero?
    @smoke = Smoke.new(45) if @args.state.tick_count.zero?

    @args.state.current ||= :title
  end

  def tick
    @args.gtk.stop_music unless @args.state.current == :title

    case @args.state.current
    when :title
      draw_title
      @args.outputs.sounds << 'sounds/title.ogg'
    when :running
      draw_ground
      draw_player
      calc
      check_land
    when :upscore
      update_score
    when :lvlup
      lvlup
    when :gameover
      gameover
    end
  end

  def calc
    @fuel -= 1 if @args.state.tick_count % 4 == 0
  end

  def reset
    @args.state.current = :title
    @explode.deactivate
    @smoke.deactivate
    @pad = make_pad
    @gnd = Ground.new(@pad)
    @player = Player.new
    @score = 0
    @fuel = 500
  end

  def make_pad
    PadSprite.new(
      rand(1280 - 90),
      rand(244),
      90,
      12
    )
  end

  def draw_title
    draw_stars
    move_stars

    title = TitleSprite.new(
      (@center_x - (TITLE_WIDTH / 2)),
      @center_y,
      TITLE_WIDTH,
      TITLE_HEIGHT,
      'sprites/spritesheet.png',
      255,
      2,
      9
    )

    @args.outputs.sprites << title

    center_text('Press X to start', 12, 1, 100, [255, 255, 255])

    @args.state.current = :running if @args.inputs.keyboard.key_down.x
  end

  def draw_ground
    draw_stars
    @gnd.draw(@args)
  end

  def draw_player
    @args.outputs.labels << [0, 710, "Fuel: #{@fuel}", 4, 0, 255, 255, 255, 255, @font]
    @player.draw(@args)
    @player.move(@args, @gravity)
  end

  def draw_stars
    @args.state.x ||= 640
    @args.state.y ||= 360
    @args.state.stars ||= 200.map do
      [
        1280 * rand,
        760 * rand,
        255 * rand,
        255 * rand,
        255 * rand
      ]
    end

    @args.outputs.solids << [0, 0, 1280, 720]
    @args.outputs.solids << @args.state.stars.map do |x, y, r, g, b|
      {
        x: x,
        y: y,
        h: 3,
        w: 3,
        r: r,
        g: g,
        b: b
      }
    end
  end

  def center_text(text, size, align, size_y, color)
    @args.outputs.labels << {
      x: @center_x - text.length,
      y: @center_y - size_y,
      text: text,
      size_enum: size,
      alignment_enum: align,
      r: color[0],
      g: color[1],
      b: color[2],
      a: 255,
      font: @font
    }
  end

  def move_stars
    @args.state.stars = @args.state.stars.map do |x, y, r, g, b|
      x += 1
      y += 1
      x = 0 if x > 1280
      y = 0 if y > 768
      [x, y, r, g, b]
    end
  end

  def explode(x, y)
    @args.audio[:explode] = { input: 'sounds/explode.wav' }
    @explode.activate(x, y)
    @smoke.activate(x, y)
  end

  def check_land
    l_x = @player.x
    r_x = @player.x + 36
    b_y = @player.y

    over_pad = ((l_x >= @pad.x) and (r_x <= (@pad.x + @pad.w)))
    on_pad = (b_y <= (@pad.y + 11))
    slow = @player.dy > -5

    if over_pad && on_pad && slow
      @args.gtk.queue_sound 'sounds/success.ogg'
      next_lvl
    elsif (over_pad && on_pad) || @fuel.zero?
      explode(l_x, b_y)
      end_game
    else
      [*l_x..r_x].each do |i|
        next unless @gnd.gnd[i] >= b_y

        explode(l_x, b_y)
        end_game
      end
    end
  end

  def end_game
    @args.state.current = :gameover
    # Sound effect
  end

  def next_lvl
    @args.state.current = :upscore
  end

  def gameover
    # Display final score.
    draw_ground
    center_text('GAME OVER', 24, 1, -200, [255, 0, 0])
    center_text("Final Score: #{@score}", 8, 1, -65, [255, 255, 255])
    center_text('Press X to reset the game', 3, 1, 0, [255, 255, 0])
    reset if @args.inputs.keyboard.key_down.x
  end

  def update_score
    draw_ground
    @args.state.current = :lvlup
    @score = @fuel + @score
  end

  def lvlup
    draw_ground
    @player.draw_flag(@args)
    center_text('LEVEL COMPLETE!', 24, 1, -200, [0, 255, 0])
    center_text("Current Score: #{@score}", 8, 1, -65, [255, 255, 255])
    center_text('Press X to continue', 3, 1, 0, [255, 255, 0])
    center_text('Press Z to quit', 3, 1, 30, [255, 255, 0])
    @args.state.current = :running if @args.inputs.keyboard.key_down.x
    if @args.inputs.keyboard.key_down.x
      regenerate
      @args.state.current = :running
    end
    if @args.inputs.keyboard.key_down.z
      @args.state.current = :title
      reset
    end
  end

  def regenerate
    @pad = make_pad
    @gnd = Ground.new(@pad)
    @player = Player.new
    @fuel = 500
  end
end
