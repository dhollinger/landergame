# frozen_string_literal: true

class Ground
  attr_reader :gnd

  def initialize(pad)
    @width = 4
    @grid_w = 1280
    @top = 244
    @btm = 55
    @grid = []
    @pad = pad
    @gnd = generate
    @colors = {
      red: [255, 0, 0],
      green: [0, 255, 0],
      blue: [0, 0, 255],
      yellow: [255, 255, 0],
      gray: [128, 128, 128],
      dpurple: [51, 0, 102],
      dgray: [64, 64, 64],
      swamp: [51, 102, 0]
    }
    @gnd_clr = random_color
  end

  def generate
    gnd = Array.new(1280, 0)

    [*@pad.x..(@pad.x + @pad.w)].each do |i|
      gnd[i] = @pad.y
    end

    [*(@pad.x + @pad.w + 1)..1279].each do |i|
      h = [*(gnd[i - 1] - 12)..(gnd[i - 1] + 12)].sample
      gnd[i] = mid(@top, h, @btm)
    end

    [*0..(@pad.x - 1)].each do |i|
      h = [*(gnd[i - 1] - 12)..(gnd[i - 1] + 12)].sample
      gnd[i] = mid(@top, h, @btm)
    end

    gnd
  end

  def mid(a, b, c)
    a = [a, b, c]
    a.sort!
    a[1]
  end

  def render_cube(args, x, y)
    args.outputs.solids << [x, y, @width, (y - 720), @gnd_clr, 255]
  end

  def random_color
    clr_codes = @colors.values

    clr_codes[rand(clr_codes.length)]
  end

  def draw(args)
    @grid_w.times do |i|
      next unless (i % @width).zero?

      render_cube(args, i, @gnd[i])
    end

    args.outputs.sprites << @pad
  end
  # def draw(args)
  #   (0..1280).each do |i|
  #     args.outputs.solids << [i, @gnd[i], i+4, @gnd[i], 255, 128, 0, 255]
  #   end
  # end
end
