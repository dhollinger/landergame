require 'app/requires.rb'

def tick(args)
  args.state.game ||= Lander.new(args)
  args.state.game.tick
end
