# frozen_string_literal: true

class PadSprite < Sprite
  def initialize(x, y, w, h, blendmode = 1)
    self.x = x
    self.y = y
    self.w = w
    self.h = h
    self.path = 'sprites/pad.png'
    self.blendmode_enum = blendmode
  end
end
