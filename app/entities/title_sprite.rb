# frozen_string_literal: true

class TitleSprite < Sprite
  def initialize(x, y, w, h, path, a, tile_x, tile_y)
    self.x = x
    self.y = y
    self.w = w
    self.h = h
    self.path = path
    self.a = a
    self.tile_x = tile_x
    self.tile_y = tile_y
  end
end
