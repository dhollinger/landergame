# frozen_string_literal: true

class Player
  attr_accessor :x, :y, :dx, :dy, :alive

  def initialize
    @x = 60
    @y = 600
    @dx = 0
    @dy = 0
    @alive = true
    @thrust = 0.2
  end

  def draw(args)
    unless args.state.current == :gameover
      args.outputs.sprites << {
        x: @x,
        y: @y,
        w: 36,
        h: 36,
        path: 'sprites/spritesheet.png',
        tile_x: 1,
        tile_y: 0,
        tile_w: 8,
        tile_h: 8,
        blendmode_enum: 1
      }
    end
  end

  def draw_flag(args)
    args.outputs.sprites << [
      {
        x: @x,
        y: @y,
        w: 36,
        h: 36,
        path: 'sprites/spritesheet.png',
        tile_x: 1,
        tile_y: 0,
        tile_w: 8,
        tile_h: 8,
        blendmode_enum: 1
      },
      {
        x: @x + 5,
        y: @y + 36,
        w: 16,
        h: 20,
        path: 'sprites/spritesheet.png',
        tile_x: 26,
        tile_y: 3,
        tile_w: 4,
        tile_h: 5,
        blendmode_enum: 1
      }
    ]
  end

  def move(args, g)
    @dy -= g

    thrust(args)

    @x += @dx
    @y += @dy

    stay_on_screen
  end

  def thrust_snd
    {
      input: 'sounds/thrust.wav'
    }
  end

  def thrust(args)
    @dx -= @thrust if args.inputs.keyboard.left
    @dx += @thrust if args.inputs.keyboard.right
    @dy += @thrust if args.inputs.keyboard.up

    if args.inputs.keyboard.key_held.left || args.inputs.keyboard.key_held.right || args.inputs.keyboard.key_held.up
      args.audio[:thrust] ||= thrust_snd
    end

    if args.inputs.keyboard.key_up.left || args.inputs.keyboard.key_up.right || args.inputs.keyboard.key_up.up
      args.audio.delete(:thrust)
    end
  end

  def stay_on_screen
    if @x.negative?
      @x = 0
      @dx = 0
    end

    if @x > (1279 - 36)
      @x = (1279 - 36)
      @dx = 0
    end

    if @y > 719 - 36
      @y = 719 - 36
      @dy = 0
    end
  end
end
